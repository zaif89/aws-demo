<div class="container">
  <div class="offset-lg-3 col-lg-6">
    <div class="border p-3 mt-3">
    <!-- <h3>Amazon PA API DEMO</h3> -->
    <?php echo form_open("product/search", ["id" => "product_search_form"]) ?>
      <div class="form-group">
        <label for="asin">Enter ASIN</label>
        <input type="text" class="form-control" id="asin" placeholder="Please enter asin" required>
      </div>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary product-form-submit">Submit</button>
      </div>
    <form>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="product-search-result"></div>
</div>
