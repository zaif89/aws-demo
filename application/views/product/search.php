<p>&nbsp;</p>
<?php if( $response ):?>
  <h4>Auto suggestion data for: <?php echo $product_title; ?></h4>
  <table class="table table-striped">
    <thead>
      <th>Sr</th>
      <th>Suggestions</th>
    </thead>
    <tbody>
      <?php $i = 0; ?>
      <?php foreach( $result as $r ): ?>
        <tr>
          <td><?php echo ++$i; ?></td>
          <td><?php echo $r; ?></td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger" role="alert">
    <?php echo $result; ?>
  </div>
<?php endif ?>
