<?php
class Product_model extends CI_Model {
  /*
  * Method: get_api_url
  */
  public function get_api_url($search) {
    $secret_key = AWS_SECRET_KEY;
    $endpoint = "webservices.amazon.com";
    $uri = "/onca/xml";
    $params = array(
      "Service" => "AWSECommerceService",
      "Operation" => "ItemLookup",
      "AWSAccessKeyId" => AWS_PUBLIC_KEY,
      "AssociateTag" => AWS_ASSOCIATE_TAG,
      "ItemId" => $search, #"B00YT13T3Q",
      "ResponseGroup" => "Small"
    );
    if (!isset($params["Timestamp"])) {
      $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
    }
    ksort($params);
    $pairs = array();
    foreach ($params as $key => $value) {
      array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
    }
    $canonical_query_string = join("&", $pairs);
    $string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;
    $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $secret_key, true));
    $request_url = 'http://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);
    return $request_url;
  }

  public function search($url) {
    $error = false;
    $file = file_get_contents($url);
    $xml = new DOMDocument();
    $xml->loadXML($file);
    $items = $xml->getElementsByTagName("Items")[0];

    foreach ( $items->getElementsByTagName("Error") as $item )    {
      $error = true;
    }

    if( $error ) {
      return false;
    } else {
      $item = $items->getElementsByTagName("ItemAttributes")[0];
      return $item->getElementsByTagName("Title")[0]->nodeValue.PHP_EOL;
    }
  }

  public function get_auto_complete_suggestions( $title ) {
    $suggestions = [];
    $title = preg_replace('/[0-9]+/', '', $title);
    $words = explode(" ", $title);
    $uri = "http://completion.amazon.com/search/complete?search-alias=aps&client=amazon-search-ui&mkt=1&q=";
    foreach( $words as $word ) {
      $r = json_decode( file_get_contents($uri.$word) );
      if( $r && isset($r[1]) ) {
        $suggestions[] = $r[1];
      }
    }
    $suggestions = $this->flatten($suggestions);
    return $suggestions;
  }

  function flatten(array $array) {
    $return = array();
    array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
    return $return;
  }
}
