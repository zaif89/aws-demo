<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

  /**
  * Method: index
  */
  public function index() {
    $this->load->view("header");
    $this->load->view('product/index');
    $this->load->view("footer");
  }

  /**
  * Method: search
  * Description: method to search amazon using the PA Api and return the keywords
  * @params asin
  */
  public function search() {
    $data = [];
    if( $this->input->method() == "post" ) {
      $data['asin'] = $asin = $this->input->post("asin");
      $this->load->model("product_model");
      $url = $this->product_model->get_api_url($asin);
      $title = $this->product_model->search($url);
      if( $title ) {
        $suggestions = $this->product_model->get_auto_complete_suggestions($title);
        $data['response'] = true;
        $data['product_title'] = $title;
        $data['result'] = $suggestions;
      } else {
        $data['response'] = false;
        $data['result'] = "Invalid ASIN - ".$asin;
      }
      $this->load->view('product/search', $data);
    } else {
      die("Invalid request");
    }
  }
}
