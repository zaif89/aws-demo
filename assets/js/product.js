$(function() {
  $("#product_search_form").on("submit", function(e) {
    e.preventDefault();
    $(".product-search-result").html("Please wait.. We are fetching data.")
    $(".product-form-submit").attr("disabled", true).text("Searching..")
    var url  = $(this).attr("action")
    var asin = $("#asin").val()

    $.post(url, {asin: asin}, function(html) {
      $(".product-search-result").html(html)
      $(".product-form-submit").removeAttr("disabled").text("Search")
    })
  })
})
